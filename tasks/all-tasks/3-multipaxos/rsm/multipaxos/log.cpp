#include <rsm/multipaxos/log.hpp>

#include <whirl/cereal/serialize.hpp>

namespace multipaxos {

PersistentLog::PersistentLog(whirl::ILocalStorageBackendPtr storage,
                             const std::string& name)
    : impl_(std::move(storage), MakeLogName(name)) {
}

LogEntry PersistentLog::Read(size_t index) {
  auto entry = impl_.TryGet(MakeKey(index));
  if (entry.has_value()) {
    return *entry;
  }
  return LogEntry::Empty();
}

void PersistentLog::Write(size_t index, LogEntry entry) {
  impl_.Set(MakeKey(index), entry);
}

bool PersistentLog::IsEmpty(size_t index) const {
  return !impl_.Has(MakeKey(index));
}

std::string PersistentLog::MakeKey(size_t index) {
  return std::to_string(index);
}

std::string PersistentLog::MakeLogName(const std::string& name) {
  return std::string("log-") + name;
}

PersistentLogPtr OpenLog(ILocalStorageBackendPtr storage,
                         const std::string& name) {
  return std::make_shared<PersistentLog>(std::move(storage), name);
}

}  // namespace multipaxos
