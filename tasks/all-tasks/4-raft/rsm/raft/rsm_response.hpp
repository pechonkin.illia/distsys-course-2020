#pragma once

#include <rsm/raft/bytes.hpp>
#include <rsm/raft/server_id.hpp>

#include <whirl/cereal/serialize.hpp>
#include <whirl/cereal/empty_message.hpp>

#include <cereal/types/variant.hpp>

#include <variant>

namespace raft {

// Response from RAFT server

//////////////////////////////////////////////////////////////////////

// 1) Redirect to leader

struct RedirectToLeader {
  ServerId id;

  WHIRL_SERIALIZE(id)
};

//////////////////////////////////////////////////////////////////////

// 2) Retry to another replica

using NotALeader = whirl::EmptyMessage;

//////////////////////////////////////////////////////////////////////

// 3) Serialized operation response

using OpResponseBytes = Bytes;

//////////////////////////////////////////////////////////////////////

// Redirect / retry / serialized operation response
using RSMResponse = std::variant<RedirectToLeader, NotALeader, OpResponseBytes>;

}  // namespace raft
