# Задачи

1. [Replicated KV Storage (ABD algorithm)](1-kv)
2. [Single-Decree Paxos](2-paxos)
3. [RSM via Multi-Paxos](3-multipaxos)
4. [RSM via RAFT](4-raft)
