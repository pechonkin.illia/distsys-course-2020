cmake_minimum_required(VERSION 3.9)

# Tasks

add_subdirectory(1-kv)
add_subdirectory(2-paxos)
add_subdirectory(3-multipaxos)
add_subdirectory(4-raft)
