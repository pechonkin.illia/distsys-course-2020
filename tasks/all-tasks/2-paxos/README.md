# Single-Decree Paxos

## Задача консенсуса

Реализуйте RPC-сервис `Proposer` с единственным методом `Value Propose(Value input)`.

### Свойства

| Свойство | Описание |
| - | - |
| _Agreement_ | Все вызовы `Propose` возвращают одно и то же значение. |
| _Validity_ | Возвращать можно только одно из предложенных значений. |
| _Termination_ | Каждый вызов `Propose` должен завершаться. |

См. [SafetyChecker](./consensus/checker.hpp).

## Алгоритм

Для решения консенсуса используйте алгоритм _Single-Decree-Paxos_ (далее – просто _Paxos_):

- [The Part-Time Parliament](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf)
- [Paxos Made Simple](https://lamport.azurewebsites.net/pubs/paxos-simple.pdf)
- [TLA+ Spec](https://github.com/tlaplus/Examples/blob/master/specifications/Paxos/Paxos.tla)

## Роли

В алгоритме Paxos выделены следующие _роли_:

* _Proposer_ – активная роль, координирует выбор значения
* _Acceptor_ - пассивная роль, голосует за (или отвергает) предложения, выдвигаемые _Proposer_-ами
* _Learner_ – опциональная роль, отслеживает отданные _Acceptor_-ами голоса и определяет выбранное (_chosen_) значение

Реализуйте эти роли в виде отдельных RPC-сервисов.

### Замечание

Алгоритм _Paxos_ требует, чтобы группировка _Acceptor_-ов была зафиксирована (они образуют кворумы), но допускает произвольное число _Proposer_-ов.

В нашем случае _Acceptor_-ы – это узлы системы, а каждый RPC `Proposer.Propose` – это новый _Proposer_. 

## Шаблон решения

[Вот он!](paxos/node)
