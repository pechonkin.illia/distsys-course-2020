#pragma once

#include <kv/types.hpp>

#include <whirl/node/node_base.hpp>
#include <whirl/node/logging.hpp>
#include <whirl/cereal/serialize.hpp>

#include <whirl/rpc/use/service_base.hpp>

// Concurrency
#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>
#include <await/fibers/sync/mutex.hpp>
#include <await/futures/combine/quorum.hpp>

// std::string serialization support
#include <cereal/types/string.hpp>
// fmt::format
#include <fmt/ostream.h>

namespace kv {

using namespace await::fibers;
using namespace whirl;

//////////////////////////////////////////////////////////////////////

// Реплики хранят версионированные значения

using Timestamp = size_t;

struct StampedValue {
  Value value;
  Timestamp ts;

  static StampedValue NoValue() {
    return {0, 0};
  }

  // Serialization support for local storage and RPC
  WHIRL_SERIALIZE(value, ts)
};

// For logging
inline std::ostream& operator<<(std::ostream& out, const StampedValue& v) {
  out << "{" << v.value << ", ts: " << v.ts << "}";
  return out;
}

//////////////////////////////////////////////////////////////////////

// KV storage node

class KVNode : public rpc::ServiceBase<KVNode>,
               public NodeBase,
               public std::enable_shared_from_this<KVNode> {
 public:
  KVNode(NodeServices runtime)
      : NodeBase(std::move(runtime)), kv_(StorageBackend(), "kv") {
  }

 protected:
  // NodeBase
  void RegisterRPCServices(const rpc::IServerPtr& rpc_server) override {
    rpc_server->RegisterService("KV", shared_from_this());
  }

  // ServiceBase
  void RegisterRPCMethods() override {
    // Coordinator RPC handlers
    RPC_REGISTER_METHOD(Set);
    RPC_REGISTER_METHOD(Get);

    // Storage replica RPC handlers
    RPC_REGISTER_METHOD(LocalWrite);
    RPC_REGISTER_METHOD(LocalRead);
  }

  // Public methods: Set and Get

  void Set(Key key, Value value) {
    Timestamp write_ts = ChooseWriteTimestamp();
    NODE_LOG_INFO("Write timestamp: {}", write_ts);

    std::vector<Future<void>> writes;
    for (size_t i = 0; i < PeerCount(); ++i) {
      writes.push_back(
          PeerChannel(i).Call("KV.LocalWrite", key, StampedValue{value, write_ts}));
    }

    // Await acks from majority of replicas
    Await(Quorum(std::move(writes), Majority())).ExpectOk();
  }

  Value Get(Key key) {
    std::vector<Future<StampedValue>> reads;

    // Broadcast KV.LocalRead request
    for (size_t i = 0; i < PeerCount(); ++i) {
      reads.push_back(PeerChannel(i).Call("KV.LocalRead", key));
    }

    // Await responses from majority of replicas

    // Steps:
    // 1) Combine futures from read RPC-s to single quorum future
    Future<std::vector<StampedValue>> quorum_reads =
        Quorum(std::move(reads), Majority());
    // 2) Block current fiber until quorum collected
    Result<std::vector<StampedValue>> results = Await(std::move(quorum_reads));
    // 3) Unpack vector or throw error
    auto values = results.Value();

    // Or just combine all steps to:
    // auto values = Await(Quorum(std::move(reads), Majority())).Value()

    for (size_t i = 0; i < values.size(); ++i) {
      NODE_LOG_INFO("{}-th value in read quorum: {}", i + 1, values[i]);
    }

    auto most_recent = FindMostRecentValue(values);
    return most_recent.value;
  }

  // Internal storage methods

  void LocalWrite(Key key, StampedValue stamped_value) {
    std::lock_guard guard(mutex_);

    std::optional<StampedValue> local = kv_.TryGet(key);

    if (!local.has_value()) {
      // First write for this key
      LocalUpdate(key, stamped_value);
    } else {
      // Write timestamp > timestamp of locally stored value
      if (stamped_value.ts > local->ts) {
        LocalUpdate(key, stamped_value);
      }
    }
  }

  void LocalUpdate(Key key, StampedValue stamped_value) {
    NODE_LOG_INFO("Write '{}' -> {}", key, stamped_value);
    kv_.Set(key, stamped_value);
  }

  StampedValue LocalRead(Key key) {
    std::lock_guard guard(mutex_);  // Blocks fiber, not thread!
    return kv_.GetOr(key, StampedValue::NoValue());
  }

  Timestamp ChooseWriteTimestamp() {
    // Local wall clock may be out of sync with other replicas
    // Use TrueTime (TrueTime() method)
    return WallTimeNow();
  }

  // Find value with largest timestamp
  StampedValue FindMostRecentValue(
      const std::vector<StampedValue>& values) const {
    auto candidate = values[0];
    for (size_t i = 1; i < values.size(); ++i) {
      if (values[i].ts > candidate.ts) {
        candidate = values[i];
      }
    }
    return candidate;
  }

  // Quorum size: strict majority
  size_t Majority() const {
    return PeerCount() / 2 + 1;
  }

 private:
  // Local persistent K/V storage
  // strings -> StampedValues
  LocalKVStorage<StampedValue> kv_;
  // Fiber-aware mutex
  await::fibers::Mutex mutex_;  // Guards accesses to kv_
};

}  // namespace kv
